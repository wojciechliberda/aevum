Please fix all the issues you've found.

New requirements:
1. Make buttons centered horizontally.
2. Time should be presented in the following format: HH:MM:SS (H - hours, M - minutes, S - seconds)
3. Exiting the app while the timer is running should not stop time measurement. When user navigates back to the app, the timer should continue to run.
4. When user exits the app while the timer is not running, and then navigates back to the app, the previous time value should be displayed.
5. User can measure lap times. When timer is running and user taps "Lap" button, the time passed since last lap is added to a list displayed below buttons.
6. Handle orientation change.

You should take into account this being a larger project having many views and components. In other words try to express your thinking about the project structure but only include the code/assets relevant to the timer.