package com.yougov.exercise.aevum.aevum

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.SmallTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.yougov.exercise.aevum.aevum.ui.main.MainActivity
import com.yougov.exercise.aevum.aevum.ui.main.MainViewModel
import com.yougov.exercise.aevum.aevum.models.LapModel
import com.yougov.exercise.aevum.aevum.models.TimeModel
import junit.framework.Assert.assertTrue
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import java.lang.Thread.sleep

@RunWith(AndroidJUnit4::class)
@SmallTest
class MainActivityTest {
    @get:Rule
    val rule: InstantTaskExecutorRule = InstantTaskExecutorRule()
    @JvmField
    @Rule
    var activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    @Rule
    @JvmField
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @Mock
    lateinit var viewModel: MainViewModel
    val time = MutableLiveData<TimeModel>()
    val laps = MutableLiveData<MutableList<LapModel>>()

    @Before
    fun setUp() {
        `when`(viewModel.time).thenReturn(time)
        `when`(viewModel.lapTimes).thenReturn(laps)
    }

    @Test
    fun `testStartPauseTextChange`() {
        onView(withId(R.id.btnStartPause)).check(matches(withText(R.string.start)))
        onView(withId(R.id.btnStartPause)).perform(click())
        onView(withId(R.id.btnStartPause)).check(matches(withText(R.string.stop)))
        onView(withId(R.id.btnStartPause)).perform(click())
        onView(withId(R.id.btnStartPause)).check(matches(withText(R.string.start)))
    }

    @Test
    fun `testResetTextchange`() {
        onView(withId(R.id.btnStartPause)).check(matches(withText(R.string.start)))
        onView(withId(R.id.btnStartPause)).perform(click())
        onView(withId(R.id.btnStartPause)).check(matches(withText(R.string.stop)))
        onView(withId(R.id.btnReset)).perform(click())
        onView(withId(R.id.btnStartPause)).check(matches(withText(R.string.start)))
    }

    @Test
    fun `testLapVisibility`() {
        onView(withId(R.id.btnLap)).check(matches(not(isDisplayed())))
        onView(withId(R.id.btnStartPause)).perform(click())
        onView(withId(R.id.btnLap)).check(matches(isDisplayed()))
        onView(withId(R.id.btnStartPause)).perform(click())
        onView(withId(R.id.btnLap)).check(matches(not(isDisplayed())))
        onView(withId(R.id.btnStartPause)).perform(click())
        onView(withId(R.id.btnLap)).check(matches(isDisplayed()))
        onView(withId(R.id.btnReset)).perform(click())
        onView(withId(R.id.btnLap)).check(matches(not(isDisplayed())))
    }

    @Test
    fun `testTimeDisplay`() {
        var text = activityRule.activity.resources.getString(R.string.time_format, 0, 0, 0)
        onView(withId(R.id.tvValue)).check(matches(withText(text)))
        onView(withId(R.id.btnStartPause)).perform(click())
        sleep(2000)
        text = activityRule.activity.resources.getString(R.string.time_format, 0, 0, 2)
        onView(withId(R.id.tvValue)).check(matches(withText(text)))
    }


    @Test
    fun `testLaps`() {
        onView(withId(R.id.btnStartPause)).perform(click())
        onView(withId(R.id.btnLap)).perform(click())
        onView(withId(R.id.btnLap)).perform(click())
        activityRule.activity.findViewById<RecyclerView>(R.id.recyclerView).let {
            val adapter = it.adapter
            assertTrue(adapter.itemCount == 2)
        }
    }
}
