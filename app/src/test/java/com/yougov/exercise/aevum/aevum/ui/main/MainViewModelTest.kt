package com.yougov.exercise.aevum.aevum.ui.main

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockito_kotlin.*
import com.yougov.exercise.aevum.aevum.data.PreferenceHelper
import com.yougov.exercise.aevum.aevum.models.TimeModel
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.junit.After
import org.junit.Before

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.TimeUnit

class MainViewModelTest {
    @get:Rule
    val rule: InstantTaskExecutorRule = InstantTaskExecutorRule()
    lateinit var testSubject: MainViewModel
    val testScheduler = TestScheduler()
    val preferenceHelper : PreferenceHelper = mock()

    companion object {
        val time = 20L
        val systemCurrent = 30L
    }

    @Before
    fun setUp() {
        testSubject = MainViewModel(preferenceHelper)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { ignore -> testScheduler }
        whenever(preferenceHelper.getLastTimeStop()).thenReturn(systemCurrent)
        whenever(preferenceHelper.getTime()).thenReturn(time)
    }

    @After
    fun tearsUp(){
        RxJavaPlugins.setComputationSchedulerHandler(null)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(null)
    }

    @Test
    fun `subject cannot be null`() {
        assertNotNull(testSubject)
    }

    @Test
    fun `should start Timer when button clicked`() {
        assertTrue(testSubject.isStarted.value == false)
        testSubject.startPauseClicked()
        increaseTime(1)
        assertTrue(testSubject.time.value == TimeModel(0,0,1))
        assertTrue(testSubject.isStarted.value == true)
    }

    @Test
    fun `should start and stop Timer when button clicked two times`() {
        assertTrue(testSubject.isStarted.value == false)
        testSubject.startPauseClicked()
        assertTrue(testSubject.isStarted.value == true)
        increaseTime(1)
        testSubject.startPauseClicked()
        assertTrue(testSubject.isStarted.value == false)
        assertTrue(testSubject.time.value == TimeModel(0,0,1))
    }

    @Test
    fun `should stop Timer and reset Time value when reset clicked`() {
        assertTrue(testSubject.isStarted.value == false)
        testSubject.startPauseClicked()
        assertTrue(testSubject.isStarted.value == true)
        increaseTime(1)
        assertTrue(testSubject.time.value == TimeModel(0,0,1))
        testSubject.resetClicked()
        assertTrue(testSubject.isStarted.value == false)
        assertTrue(testSubject.time.value == TimeModel(0,0,0))
    }

    @Test
    fun `should add lap Time when lap clicked`(){
        assertTrue(testSubject.isStarted.value == false)
        testSubject.startPauseClicked()
        assertTrue(testSubject.isStarted.value == true)
        increaseTime(1)
        testSubject.lapClicked()
        assertTrue(testSubject.lapTimes.value!!.size == 1)
        increaseTime(1)
        testSubject.lapClicked()
        increaseTime(2)
        testSubject.lapClicked()
        increaseTime(3660)
        testSubject.lapClicked()
        assertTrue(testSubject.lapTimes.value!!.size == 4)
        testSubject.lapTimes.value!![0].timeModel.apply {
            testTimeModel(0, 0, 1, this)
        }
        testSubject.lapTimes.value!![1].timeModel.apply {
            testTimeModel(0, 0, 1, this)
        }
        testSubject.lapTimes.value!![2].timeModel.apply {
            testTimeModel(0, 0, 2, this)
        }
        testSubject.lapTimes.value!![3].timeModel.apply {
            testTimeModel(1, 1, 0, this)
        }
    }

   @Test
   fun `when timer not running should be no interaction with preferenceHelper`() {
       testSubject.onStart(systemCurrent)
       verifyNoMoreInteractions(preferenceHelper)
       testSubject.time.value = TimeModel(0,0,0)
   }

    @Test
    fun `when timer running should set time from preferences`() {
        testSubject.startPauseClicked()
        testSubject.onStart(40L)
        verify(preferenceHelper, times(1)).getTime()
        testTimeModel(0,0,30, testSubject.time.value!!)
    }

    @Test
    fun `when Timer running and onStop called should save time in preferences`() {
        testSubject.startPauseClicked()
        increaseTime(time)
        testSubject.onStop(systemCurrent)
        verify(preferenceHelper, times(1)).setTime(time)
        verify(preferenceHelper, times(1)).setLastTimeStop(systemCurrent)
    }

    @Test
    fun `when timer not running and onStop called should not interace with preferences`() {
        testSubject.onStop(systemCurrent)
        verifyNoMoreInteractions(preferenceHelper)
    }

    private fun testTimeModel(hours: Int, minutes: Int, seconds: Int, timeModel: TimeModel) {
        timeModel.apply {
            assertTrue(hours == this.hours)
            assertTrue(minutes == this.minutes)
            assertTrue(seconds == this.seconds)
        }
    }

    private fun increaseTime(time: Long) {
        testScheduler.advanceTimeBy(time, TimeUnit.SECONDS)
    }


}