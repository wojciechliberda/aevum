package com.yougov.exercise.aevum.aevum.ui.main

import android.content.Context
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.yougov.exercise.aevum.aevum.R
import com.yougov.exercise.aevum.aevum.models.LapModel

class LapAdapter(var list: MutableList<LapModel>, val context: Context) : RecyclerView.Adapter<LapAdapter.LapViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LapViewHolder =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_lap, parent, false).run {
                LapViewHolder(this)
            }

    override fun getItemCount(): Int = list.size


    override fun onBindViewHolder(holder: LapViewHolder, position: Int) {
        holder.apply {
            tvNumber.text = (position + 1).toString()
            list[position].timeModel.apply {
                tvTime.text = context.resources.getString(R.string.time_format, hours, minutes, seconds)
            }
        }
    }

    fun setLaps(laps: MutableList<LapModel>) {
        val diffResult = DiffUtil.calculateDiff(DiffLap(list, laps))
        list.apply {
            clear()
            addAll(laps)
        }
        diffResult.dispatchUpdatesTo(this)
    }


    inner class LapViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvNumber = view.findViewById<TextView>(R.id.tvNumber)!!
        val tvTime = view.findViewById<TextView>(R.id.tvTime)!!
    }

}