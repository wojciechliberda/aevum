package com.yougov.exercise.aevum.aevum.ui.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.yougov.exercise.aevum.aevum.R
import com.yougov.exercise.aevum.aevum.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: MainViewModelFactory
    lateinit var viewModel: MainViewModel
    private lateinit var lapAdapter: LapAdapter
    override fun getLayoutId(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        setRecyclerView()
        setBindings()
        setObservers()
    }

    private fun setRecyclerView() {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            lapAdapter = LapAdapter(mutableListOf(), context)
            recyclerView.adapter = lapAdapter
        }
    }

    private fun setBindings() {
        btnStartPause.setOnClickListener { viewModel.startPauseClicked() }
        btnReset.setOnClickListener { viewModel.resetClicked() }
        btnLap.setOnClickListener { viewModel.lapClicked() }
    }

    override fun onStart() {
        super.onStart()
        viewModel.onStart(System.currentTimeMillis() / 1000)
    }

    override fun onStop() {
        super.onStop()
        viewModel.onStop(System.currentTimeMillis() / 1000)
    }

    private fun setObservers() {
        viewModel.isStarted.observe(this, Observer {
            it?.let {
                if (it) {
                    btnStartPause.text = resources.getText(R.string.stop)
                    btnLap.visibility = View.VISIBLE
                } else {
                    btnStartPause.text = resources.getText(R.string.start)
                    btnLap.visibility = View.INVISIBLE
                }
            }
        })
        viewModel.time.observe(this, Observer {
            it?.apply {
                tvValue.text = resources.getString(R.string.time_format, hours, minutes, seconds)
            }
        })
        viewModel.lapTimes.observe(this, Observer {
            it!!.let {
                lapAdapter.setLaps(it)
            }
        })
    }

}