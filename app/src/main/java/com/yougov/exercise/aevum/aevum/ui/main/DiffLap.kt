package com.yougov.exercise.aevum.aevum.ui.main

import android.support.v7.util.DiffUtil
import com.yougov.exercise.aevum.aevum.models.LapModel

class DiffLap(val oldList: List<LapModel>, val newList: List<LapModel>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]
        return oldItem.uuid == newItem.uuid
    }


    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] == newList[newItemPosition]


}