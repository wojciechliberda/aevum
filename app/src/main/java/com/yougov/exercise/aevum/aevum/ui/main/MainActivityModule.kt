package com.yougov.exercise.aevum.aevum.ui.main

import com.yougov.exercise.aevum.aevum.data.PreferenceHelper
import com.yougov.exercise.aevum.aevum.ui.base.BaseActivityModule
import com.yougov.exercise.aevum.aevum.di.scopes.PerActivity
import dagger.Module
import dagger.Provides

@Module(includes = [BaseActivityModule::class])
class MainActivityModule {


    @Provides
    @PerActivity
    fun provideMainViewModel(preferenceHelper: PreferenceHelper) = MainViewModelFactory(preferenceHelper)

}