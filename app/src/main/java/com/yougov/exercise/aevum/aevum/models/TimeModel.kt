package com.yougov.exercise.aevum.aevum.models

data class TimeModel(val hours: Int, val minutes: Int, val seconds: Int) {
}