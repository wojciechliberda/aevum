package com.yougov.exercise.aevum.aevum.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.yougov.exercise.aevum.aevum.data.PreferenceHelper
import com.yougov.exercise.aevum.aevum.data.PreferenceHelperImpl
import com.yougov.exercise.aevum.aevum.di.qualifiers.ForApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ActivityBuilder::class])
class AppModule {

    @Singleton
    @Provides
    @ForApplication
    fun provideContext(app: Application): Context = app.applicationContext

    @Singleton
    @Provides
    fun provideSharedPreference(@ForApplication context: Context)= PreferenceManager.getDefaultSharedPreferences(context)!!

    @Singleton
    @Provides
    fun providePreferenceHelper(sharedPreferences: SharedPreferences): PreferenceHelper = PreferenceHelperImpl(sharedPreferences)
}