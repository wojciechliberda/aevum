package com.yougov.exercise.aevum.aevum.models

import java.util.*

data class LapModel(val timeModel: TimeModel,val uuid: UUID = UUID.randomUUID())
