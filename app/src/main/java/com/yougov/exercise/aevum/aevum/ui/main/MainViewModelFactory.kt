package com.yougov.exercise.aevum.aevum.ui.main

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.yougov.exercise.aevum.aevum.data.PreferenceHelper

@Suppress("UNCHECKED_CAST")
class MainViewModelFactory(val preferenceHelper: PreferenceHelper): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(preferenceHelper) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")    }
}