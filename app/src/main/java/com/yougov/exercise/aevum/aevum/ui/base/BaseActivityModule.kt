package com.yougov.exercise.aevum.aevum.ui.base

import android.app.Activity
import android.content.Context
import com.yougov.exercise.aevum.aevum.di.qualifiers.ForActivity
import dagger.Binds
import dagger.Module

@Module
abstract class BaseActivityModule {


    @Binds
    @ForActivity
    abstract fun bindContext(activity: Activity): Context

}