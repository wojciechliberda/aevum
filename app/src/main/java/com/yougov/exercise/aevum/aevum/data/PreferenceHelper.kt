package com.yougov.exercise.aevum.aevum.data

interface PreferenceHelper  {
    fun getTime(): Long
    fun setTime(value: Long)
    fun getLastTimeStop(): Long
    fun setLastTimeStop(value: Long)
}