package com.yougov.exercise.aevum.aevum.ui.main

import android.arch.lifecycle.MutableLiveData
import com.yougov.exercise.aevum.aevum.OpenForTesting
import com.yougov.exercise.aevum.aevum.data.PreferenceHelper
import com.yougov.exercise.aevum.aevum.ui.base.BaseViewModel
import com.yougov.exercise.aevum.aevum.models.LapModel
import com.yougov.exercise.aevum.aevum.models.TimeModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit


@OpenForTesting
class MainViewModel constructor(val preferenceHelper: PreferenceHelper) : BaseViewModel() {
    private var timerDisposable: Disposable? = null
    var isStarted = MutableLiveData<Boolean>().apply {
        value = false
    }
    var time = MutableLiveData<TimeModel>().apply {
        value = TimeModel(0, 0, 0)
    }
    var lapTimes = MutableLiveData<MutableList<LapModel>>().apply {
        value = mutableListOf()
    }

    private var seconds = 0
    private var lastLapSeconds = 0


    override fun onCleared() {
        super.onCleared()
        timerDisposable?.dispose()
    }

    fun startPauseClicked() {
        if (isStarted.value!!) {
            stopTimer()
            isStarted.value = false
        } else {
            startTimer()
            isStarted.value = true
        }
    }

    private fun startTimer() {
        timerDisposable = Observable.interval(1000000, TimeUnit.MICROSECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    seconds += 1
                    setTime()
                })
    }

    private fun setTime() {
        calculateTimeFromSeconds(seconds).let {
            time.value = it
        }
    }

    private fun stopTimer() {
        timerDisposable?.let {
            if (!it.isDisposed) it.dispose()
        }
    }

    fun resetClicked() {
        if (isStarted.value == true) {
            isStarted.value = false
            stopTimer()
        }
        seconds = 0
        lastLapSeconds = 0
        setTime()
    }

    fun lapClicked() {
        val lapSeconds = seconds - lastLapSeconds
        lastLapSeconds = seconds
        calculateTimeFromSeconds(lapSeconds).let {
            lapTimes.value!!.add(LapModel(it))
            lapTimes.value = lapTimes.value
        }
    }

    fun onStart(currentTime: Long) {
        if(isStarted.value == true) {
            val differentTime = currentTime - preferenceHelper.getLastTimeStop()
            seconds = preferenceHelper.getTime().toInt() + differentTime.toInt()
            setTime()
        }
    }

    fun onStop(currentTime: Long) {
        if(isStarted.value == true) {
            preferenceHelper.setTime(seconds.toLong())
            preferenceHelper.setLastTimeStop(currentTime)
        }
    }

    private fun calculateTimeFromSeconds(lapSeconds: Int): TimeModel =
            (lapSeconds / 3600).let { hours ->
                ((lapSeconds % 3600) / 60).let { minutes ->
                    (lapSeconds % 60).let { seconds ->
                        TimeModel(hours, minutes, seconds)
                    }
                }
            }


}