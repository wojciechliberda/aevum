package com.yougov.exercise.aevum.aevum.di

import com.yougov.exercise.aevum.aevum.di.scopes.PerActivity
import com.yougov.exercise.aevum.aevum.ui.main.MainActivity
import com.yougov.exercise.aevum.aevum.ui.main.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @PerActivity
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun contributeMainActivity(): MainActivity
}