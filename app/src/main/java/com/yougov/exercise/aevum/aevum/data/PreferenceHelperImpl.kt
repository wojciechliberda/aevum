package com.yougov.exercise.aevum.aevum.data

import android.content.SharedPreferences
import javax.inject.Inject

class PreferenceHelperImpl @Inject constructor(val sharedPreferences: SharedPreferences) : PreferenceHelper {

    companion object {
        const val TIME_PREFERENCE = "TIME_PREFERENCE"
        const val LAST_TIME_PREFERENCE = "LAST_TIME_PREFERENCE"
    }

    override fun getTime(): Long =
        sharedPreferences.getLong(TIME_PREFERENCE, 0L)


    override fun setTime(value: Long) {
        sharedPreferences.edit().putLong(TIME_PREFERENCE, value).apply()
    }

    override fun getLastTimeStop(): Long =
        sharedPreferences.getLong(LAST_TIME_PREFERENCE, 0L)


    override fun setLastTimeStop(value: Long) {
        sharedPreferences.edit().putLong(LAST_TIME_PREFERENCE, value).apply()
    }
}